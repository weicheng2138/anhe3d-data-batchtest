package com.anhe3d.controller;

import com.anhe3d.domain.Shop;
import com.anhe3d.domain.ShopOrder;
import com.anhe3d.repository.EmployeeRepository;
import com.anhe3d.repository.ShopMenuRepository;
import com.anhe3d.repository.ShopOrderRepository;
import com.anhe3d.repository.ShopRepository;
import com.anhe3d.validator.ShopOrderValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by kevinhung on 3/23/16.
 */
@Controller
@RequestMapping(path = "/menu")
public class MenuController {

    @Autowired
    ShopRepository shopRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    ShopMenuRepository shopMenuRepository;

    @Autowired
    ShopOrderRepository shopOrderRepository;

    @Autowired
    ShopOrderValidator shopOrderValidator;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getMenuInfo(@ModelAttribute("shopName")String selectedShop) {

        //Employee options
        HashMap<String, String> employeeMap = new HashMap<>();
        employeeRepository.findAll().forEach(employee -> employeeMap.put(employee.getEmployeeId(), employee.getFirstName()));

        //Selected shop
        Shop shop = shopRepository.findByShopName(selectedShop);

        //Shop Menu options
        HashMap<Long, String > itemMap = new HashMap<>();
        shopMenuRepository.findAllByShop(shop).forEach(item -> itemMap.put(item.getItemId(), item.getItemName()));

        ModelAndView modelAndView = new ModelAndView("menu");
        modelAndView.addObject("shopOrder", new ShopOrder());
        modelAndView.addObject("employeeMap", employeeMap);
        modelAndView.addObject("itemMap", itemMap);
        modelAndView.addObject("shopName", shop.getShopName());

        return modelAndView;
    }

    @RequestMapping(method= RequestMethod.POST)
    @ResponseBody
    public ModelAndView form(@ModelAttribute("shopOrder")ShopOrder shopOrder, BindingResult bindingResult) {

        //Validation first
        shopOrderValidator.validate(shopOrder, bindingResult);
        if (bindingResult.hasErrors()) {
            HashMap<String, String> employeeMap = new HashMap<>();
            employeeRepository.findAll().forEach(em -> employeeMap.put(em.getEmployeeId(), em.getFirstName()));
            HashMap<Long, String> itemMap = new HashMap<>();
            shopMenuRepository.findAllByShop(shopOrder.getShopMenu().getShop()).forEach(tm -> itemMap.put(tm.getItemId(), tm.getItemName()));

            ModelAndView modelAndView = new ModelAndView("menu");
            modelAndView.addObject("employeeMap", employeeMap);
            modelAndView.addObject("itemMap", itemMap);
            modelAndView.addObject("shopName", shopOrder.getShopMenu().getShop().getShopName());
            modelAndView.addObject("shopOrder", shopOrder);

            return modelAndView;
        } else {

            Timestamp currentTime = new Timestamp(new Date().getTime());
            double totalPrice = shopOrder.getShopMenu().getItemPrice()*shopOrder.getQuantity();
            shopOrder.setOrderTime(currentTime);
            shopOrder.setTotalPrice(totalPrice);
            shopOrderRepository.save(shopOrder);

            return new ModelAndView("result");
        }
    }
}
