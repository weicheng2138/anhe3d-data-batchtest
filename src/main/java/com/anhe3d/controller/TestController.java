package com.anhe3d.controller;

import com.anhe3d.domain.ShopOrder;
import com.anhe3d.repository.EmployeeRepository;
import com.anhe3d.repository.ShopMenuRepository;
import com.anhe3d.repository.ShopOrderRepository;
import com.anhe3d.repository.ShopRepository;
import com.anhe3d.service.ShopOrderService;
import com.anhe3d.validator.ShopOrderValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;

/**
 * Created by kevinhung on 3/29/16.
 */

@RequestMapping(path = "/test")
@Controller
public class TestController{

    @Autowired
    ShopRepository shopRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    ShopMenuRepository shopMenuRepository;

    @Autowired
    ShopOrderService shopOrderService;

    @Autowired
    ShopOrderRepository shopOrderRepository;

    @Autowired
    ShopOrderValidator shopOrderValidator;


    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView menuInfo(ModelMap modelMap
//                              @ModelAttribute("employeeMap")ArrayList<Employee> employeeMap
//                              @ModelAttribute("employeeMap")HashMap<String, String> employeeMap
//                              @ModelAttribute("shopOrder")ShopOrder shopOrder
    ) {
        System.out.println("Start initiate menuInfo");

        //Employee options
        HashMap<String, String> employeeMap = new HashMap<>();
        employeeRepository.findAll().forEach(employee -> employeeMap.put(employee.getEmployeeId(), employee.getFirstName()));

//        ArrayList<Employee> employeeMap = new ArrayList<>();
//        employeeRepository.findAll().forEach(employeeMap::add);

        //Shop name present
//        Shop shop = shopRepository.findOne(1L);

        //Shop Menu options
//        List<String> itemList = new ArrayList<>();
//        shopMenuRepository.findAllByShop(shop).forEach(item -> itemList.add(item.getItemName()+" --> " + "價格："+item.getItemPrice()));

        //TextArea for remark
        String textArea = "";

        //quantity
        int quantity = 1;


//        ShopOrder shopOrder = new ShopOrder();
//        modelMap.addAttribute("shopName", shop.getShopName());
//        modelMap.addAttribute("itemList", itemList);
//        modelMap.addAttribute("quantity", quantity);
//        modelMap.addAttribute("textArea", textArea);
//        modelMap.addAttribute("shopOrder", new ShopOrder());
//        modelMap.addAttribute("employeeMap", employeeMap);

        ModelAndView modelAndView = new ModelAndView("test");
        modelAndView.addObject("employeeMap", employeeMap);
        modelAndView.addObject("shopOrder", new ShopOrder());


//        return new ModelAndView("test", "test", new ShopOrder());
        return modelAndView;
    }

    @RequestMapping(method= RequestMethod.POST)
    public ModelAndView form(
//            @RequestParam("selectedEmployee") String employeeId,
//                       @RequestParam("selectedItem") String selectedItem,
//                       @RequestParam("quantity") int quantity,
//                       @RequestParam("remark") String remark,
                       @ModelAttribute("shopOrder")ShopOrder shopOrder,
//                       @ModelAttribute("shopMenu")ShopMenu shopMenu,
//                       @ModelAttribute("employee")Employee employee,
                       ModelMap modelMap,
                       BindingResult bindingResult
    ) {

//        employee = employeeFromSelection;
//        System.out.println(employeeId.getFirstName());
//        employee = employeeRepository.findByEmployeeId(employeeId);
//        Employee employee = shopOrder.getEmployee();
//        System.out.println(employee.toString());

//        Timestamp currentTime = new Timestamp(new Date().getTime());
//        String itemName = StringUtils.substringBefore(selectedItem, " ");
//        shopMenu = shopMenuRepository.findByItemName(itemName);
//        double totalPrice = shopMenu.getItemPrice()*quantity;

//        shopOrder = new ShopOrder(employee, shopMenu, currentTime, quantity, totalPrice, remark);
//        shopOrderRepository.save(shopOrder);
        modelMap.addAttribute("shopOrder", shopOrder);
//        modelMap.addAttribute("employee", employee);
//        modelMap.addAttribute("shopMenu", shopMenu);


        shopOrderValidator.validate(shopOrder, bindingResult);

        if (bindingResult.hasErrors()) {
            HashMap<String, String> employeeMap = new HashMap<>();
            employeeRepository.findAll().forEach(em -> employeeMap.put(em.getEmployeeId(), em.getFirstName()));

            ModelAndView modelAndView = new ModelAndView("test");
            modelAndView.addObject("employeeMap", employeeMap);

            return modelAndView;
        } else {
            return new ModelAndView("test-result");
        }
    }
}
