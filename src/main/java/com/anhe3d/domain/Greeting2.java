package com.anhe3d.domain;

/**
 * Created by kevinhung on 4/7/16.
 */
public class Greeting2 {

    private Long id;

    private String text;

    public Greeting2() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
