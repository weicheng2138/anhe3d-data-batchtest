package com.anhe3d.domain;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by kevinhung on 3/24/16.
 */
@Entity
@Table(name = "SHOP_ORDER")
public class ShopOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ORDER_ID")
    private Long orderId;

    @ManyToOne
    @JoinColumn(name = "EMPLOYEE_ID")
    private Employee employee;

    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    private ShopMenu shopMenu;

    @Column(name = "ORDER_TIME")
//    @Temporal(TemporalType.TIMESTAMP)
    private Timestamp orderTime;

    @Column(name = "QUANTITY")
    private Integer quantity;

    @Column(name = "TOTAL_PRICE")
    private double totalPrice;

    @Column(name = "REMARK")
    private String remark;

    public ShopOrder() {
    }

    public ShopOrder(Employee employee, ShopMenu shopMenu, Timestamp orderTime, Integer quantity, double totalPrice, String remark) {
        this.employee = employee;
        this.shopMenu = shopMenu;
        this.orderTime = orderTime;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
        this.remark = remark;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Timestamp getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Timestamp orderTime) {
        this.orderTime = orderTime;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public ShopMenu getShopMenu() {
        return shopMenu;
    }

    public void setShopMenu(ShopMenu shopMenu) {
        this.shopMenu = shopMenu;
    }

    @Override
    public String toString() {
        return "ShopOrder{" +
                "remark='" + remark + '\'' +
                ", orderId=" + orderId +
                ", employee=" + employee +
                ", shopMenu=" + shopMenu +
                ", orderTime=" + orderTime +
                ", quantity=" + quantity +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
