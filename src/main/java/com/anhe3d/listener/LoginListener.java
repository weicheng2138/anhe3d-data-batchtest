package com.anhe3d.listener;

import com.anhe3d.service.SmtpMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

/**
 * Created by kevinhung on 3/22/16.
 */
@Component
public class LoginListener implements ApplicationListener<InteractiveAuthenticationSuccessEvent>{

    @Autowired
    private SmtpMailService smtpMailService;

    @Override
    public void onApplicationEvent(InteractiveAuthenticationSuccessEvent interactiveAuthenticationSuccessEvent) {
        try {
            smtpMailService.send("weicheng2138@gmail.com", "test mail...", "KKKKKKKKKKKKK");
            System.out.println("Login success!!!");
        } catch (Exception execption) {
            execption.printStackTrace();
        }
    }
}
