package com.anhe3d.monitor;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * Created by kevinhung on 4/1/16.
 */
@Aspect
@Component
public class ServiceMonitor {

    @Before("execution(* com.anhe3d.service.*Service.*(..))")
    public void logServiceAccess(JoinPoint joinPoint) {
        System.out.println("the args is ..." + joinPoint.getArgs()[0]);
    }

//    @Before("execution(* com.anhe3d.service.*Service.*(..)) && args(employee,..)")
//    public void logServiceAccess(Employee employee) {
//        System.out.println("the args is ..." + employee);
//    }

//    @After("execution(* com.anhe3d.service.*Service.*(..))")
//    @AfterReturning
//    @AfterThrowing
//    public void logServiceAfter(JoinPoint joinPoint) {
//        System.out.println("the args is ..." + joinPoint.getArgs()[0]);
//
//    }
}
