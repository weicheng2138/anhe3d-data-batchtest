package com.anhe3d.validator;

import com.anhe3d.domain.ShopOrder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by kevinhung on 4/4/16.
 */
@Component
public class ShopOrderValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return ShopOrder.class.equals(aClass);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        ShopOrder shopOrder = (ShopOrder) obj;

        //Validate that whether the field of obj is null or whitespace
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "quantity", "","Quantity is empty");

        //Custom validate logic
        if (
                shopOrder.getQuantity() != null
                && shopOrder.getQuantity()<1
                && shopOrder.getQuantity() == (int)shopOrder.getQuantity()) {
            System.out.println("EXECUTE ERRORS REJECT VALUE");
            errors.rejectValue("quantity", "","Quantity should larger than 1");
        }
    }
}
