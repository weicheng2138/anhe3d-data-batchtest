<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<div align="center">
<h1>ANHE3D PORTAL</h1>
</div>
<c:if test="${not empty param.error}">
    <div>錯誤登入資訊</div>
</c:if>
<c:if test="${not empty param.logout}">
    <div>你已經登出了！</div>
</c:if>
<div align="center">
<form action="${pageContext.request.contextPath}/login" method="POST">
    <table>
        <tr>
            <td>User Name:</td>
                <td>
                    <div><input type="text" name="username"/></div>
                </td>
        </tr>
        <tr>
            <td>User Password:</td>
                <td>
                    <div><input type="password" name="password"/></div>
                </td>
        </tr>
        <tr>
            <td colspan="2">
                <div><input type="submit" value="登入"/></div>
            </td>
        </tr>
    </table>
</form>
</div>
</body>
</html>