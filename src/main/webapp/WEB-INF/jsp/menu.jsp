<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%--
  Created by IntelliJ IDEA.
  User: kevinhung
  Date: 3/29/16
  Time: 12:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Test</title>
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>
<div align="center">


<h3>ANHE3D LOVELY ORDER SYSTEM</h3>


<form:form method="post" modelAttribute="shopOrder">
    <table>
        <tr>
            <td>員工名稱：</td>
            <td>
                <form:select path="employee">
                    <form:options items="${employeeMap}"  />
                </form:select>
            </td>
        </tr>
        <tr>
            <td>店家名稱：</td>
            <td>
                ${shopName}
            </td>
        </tr>
        <tr>
            <td>菜單：</td>
            <td>
                <form:select path="shopMenu">
                    <form:options items="${itemMap}" />
                </form:select>
            </td>
        </tr>
        <tr>
            <td>數量：</td>
            <td><form:input path="quantity" type="number"/></td>
        </tr>
        <tr>
            <td></td>
            <td><form:errors path="quantity" cssClass="error"/></td>
        </tr>
        <tr>
            <td>備註：</td>
            <td><form:textarea name="remark" path="remark" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Submit"/>
            </td>
        </tr>
    </table>
</form:form>
</div>

</body>
</html>
