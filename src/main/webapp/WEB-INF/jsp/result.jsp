<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: kevinhung
  Date: 3/30/16
  Time: 17:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Order Result</title>
</head>
<body>
<form:form modelAttribute="shopOrder">
<div align="center">
    <table border="0">
        <tr>
            <td colspan="2" align="center"><h2>訂購成功惹！</h2></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <h3>感謝您的訂購，以下是您的訂購資訊：</h3>
            </td>
        </tr>
        <tr>
            <td>訂購人:</td>
            <td>${shopOrder.employee.firstName}</td>
        </tr>
        <tr>
            <td>店家:</td>
            <td>${shopOrder.shopMenu.shop.shopName}</td>
        </tr>
        <tr>
            <td>訂購品項:</td>
            <td>${shopOrder.shopMenu.itemName}</td>
        </tr>
        <tr>
            <td>訂購時間:</td>
            <td>${shopOrder.orderTime}</td>
        </tr>
        <tr>
            <td>數量:</td>
            <td>${shopOrder.quantity}</td>
        </tr>
        <tr>
            <td>總金額:</td>
            <td>${shopOrder.totalPrice}</td>
        </tr>
        <tr>
            <td>備註:</td>
            <c:if test="${not empty shopOrder.remark}">
                <td>${shopOrder.remark}</td>
            </c:if>
            <c:if test="${empty shopOrder.remark}">
                <td>empty</td>
            </c:if>
        </tr>

    </table>
</div>
</form:form>
</body>
</html>
