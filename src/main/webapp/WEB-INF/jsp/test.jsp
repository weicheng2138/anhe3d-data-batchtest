<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: kevinhung
  Date: 3/29/16
  Time: 12:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Test</title>
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>
<div align="center">

    <h3>ANHE3D LOVELY TEST</h3>
    <form:form method='post' modelAttribute="shopOrder" commandName="shopOrder">
        <table>
            <td>員工名稱：</td>
            <td>
                <form:select path="employee">
                    <form:options items="${employeeMap}"   />
                </form:select>
            </td>
            <tr>
                <td>Quantity:</td>
            </tr>
            <tr>
                <td><form:input path="quantity"/></td><td><form:errors path="quantity" cssClass="error"/></td>
            </tr>
            <tr>
                <td><button type="submit">Submit</button></td>
            </tr>
        </table>
    </form:form>

    <%--<form method='post'>--%>
        <%--<table>--%>
            <%--<td>員工名稱：</td>--%>
            <%--<td>--%>
                <%--<form:select name = "selectedEmployee" path="employeeMap">--%>
                    <%--<form:options items="${employeeMap}"  />--%>
                <%--</form:select>--%>
            <%--</td>--%>


            <%--<form:form modelAttribute="shopOrder">--%>
                <%--<tr>--%>
                    <%--<td>Quantity:</td>--%>
                <%--</tr>--%>
                <%--<tr>--%>
                    <%--<td><form:input path="quantity"/></td><td><form:errors path="quantity" cssClass="error"/></td>--%>
                <%--</tr>--%>

            <%--</form:form>--%>

            <%--<tr>--%>
                <%--<td><button type="submit">Submit</button></td>--%>
            <%--</tr>--%>
        <%--</table>--%>
    <%--</form>--%>

</div>

</body>
</html>
