package com.anhe3d;

import com.anhe3d.domain.Employee;
import com.anhe3d.domain.Shop;
import com.anhe3d.domain.ShopMenu;
import com.anhe3d.domain.ShopOrder;
import com.anhe3d.repository.EmployeeRepository;
import com.anhe3d.repository.ShopMenuRepository;
import com.anhe3d.repository.ShopOrderRepository;
import com.anhe3d.repository.ShopRepository;
import com.anhe3d.service.EmployeeService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kevinhung on 3/21/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class JPATest {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ShopRepository shopRepository;

    @Autowired
    private ShopMenuRepository shopMenuRepository;

    @Autowired
    private ShopOrderRepository shopOrderRepository;

    @Autowired
    private EmployeeService employeeService;

    @Test
    public void testFindAll() {
        List<Employee> employees = new ArrayList<>();
        employeeRepository.findAll().forEach(employees::add);
        System.out.println("testFindAll");
        employees.forEach(employee -> System.out.println(employee.getLastName()));
        Assert.assertNotNull(employees);
    }

    @Test
    public void testRead() {
        Employee employee = employeeRepository.findOne(2L);
        System.out.println(employee.getLastName());
    }

    @Test
    public void testFindByCellphone() {
        System.out.println(employeeRepository.findByCellphone("0988788290").getFirstName());
    }

    @Test
    public void testFindByFirstName() {
        System.out.println(employeeRepository.findByFirstName("瑋成").getLastName());
    }

    @Test
    public void testAddEmployee() {
        Employee employee = new Employee("0009", "建輝", "吳", "0936049703");
        employeeRepository.save(employee);
    }

    @Test
    public void testDelete() {
        Employee employee = employeeRepository.findByCellphone("0936049703");
        employeeRepository.delete(employee);
    }

    @Test
    public void testShopFind() {
        List<Shop> shops = new ArrayList<>();
        shopRepository.findAll().forEach(shops::add);
        shops.forEach(shop -> System.out.println(shop.getShopName()));
    }

    @Test
    public void testShopMenuFind() {
        List<ShopMenu> shopMenus = new ArrayList<>();
        shopMenuRepository.findAll().forEach(shopMenus::add);
        shopMenus.forEach(item -> System.out.println(item.getItemName()));
    }

    @Test
    public void testShopOrderFind() {
        List<ShopOrder> shopOrders = new ArrayList<>();
        shopOrderRepository.findAll().forEach(shopOrders::add);
        shopOrders.forEach(order -> System.out.println(order.getOrderTime()));
    }

    @Test
    public void testQuery() {
        shopMenuRepository.testQuery().forEach(shopMenu -> System.out.println(shopMenu.getItemName()));
    }

    @Test
    public void testOneToMany() {
        shopRepository.testQuery();
    }

    @Test
    public void testQueryAdvaced() {
        shopMenuRepository.testQueryAdvanced().forEach(objects -> System.out.println(objects[0] + " " + objects[1]));
    }

    @Test
    public void testSaveShopOrder() {

        Date Now = new Date();
        shopOrderRepository.save(new ShopOrder(employeeRepository.findOne(1L),
                shopMenuRepository.findOne(11L),
                new Timestamp(Now.getTime()),
                10, 60000, "remarkable"));
    }

    @Test
    public void testSaveShopMenu() {
        shopMenuRepository.save(new ShopMenu(shopRepository.findOne(2L), "60盎司牛排", 6000));
    }

    @Test
    public void testAOPFuctionality() throws Exception {
        Employee employee = new Employee("0016", "Keun", "Cheng", "0988777299");
        employeeService.addEmployee(employee);
    }

    @Test
    public void updateAnEmployee() {
        Employee employee = employeeRepository.findByEmployeeId("0016");
        employee.setAddress("台灣省");
        employeeRepository.save(employee);
        System.out.println(employee.toString());
    }

}